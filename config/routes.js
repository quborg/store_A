module.exports.routes = {

  'GET /api/products': {
    controller: 'Product',
    action: 'index',
  },
  'POST /api/product': {
    controller: 'Product',
    action: 'create',
  },
  'GET /api/product/:id': {
    controller: 'Product',
    action: 'find',
  },
  'PUT /api/product/:id?': {
    controller: 'Product',
    action: 'update',
  },
  'DELETE /api/product/:id?': {
    controller: 'Product',
    action: 'destroy',
  },

}
