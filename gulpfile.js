var gulp = require('gulp'),
    browserify = require('browserify'),
    source = require('vinyl-source-stream'),
    browserSync = require('browser-sync')

gulp.task('browserify', function() {
  browserify('views/js/main.js')
    .transform('babelify')
    .bundle()
    .pipe(source('main.js'))
    .pipe(gulp.dest('assets/js'))
})

gulp.task('copy', function() {
  gulp.src('views/index.html')
    .pipe(gulp.dest('assets'))
  gulp.src('views/styles/**/*.*')
    .pipe(gulp.dest('assets/styles'))
  gulp.src('views/vendor/**/*.*')
    .pipe(gulp.dest('assets/vendor'))
  gulp.src('views/images/**/*.*')
    .pipe(gulp.dest('assets/images'))
})

gulp.task('browsersync', ['browserify', 'copy'], function() {
  browserSync.init(['.tmp/**/*.*'], {
    proxy: 'localhost:3000',
    files: ['.tmp/public/js'],
    port: 3001
  })
})

gulp.task('default',['browserify', 'copy', 'browsersync'], function() {
  return gulp.watch('views/**/*.*', ['browserify', 'copy'])
})
