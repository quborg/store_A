# Nodejs { Sails - React/Flux }


## Installation
from the root run
```
$ npm install
```
## Runing Application
```
$ sails lift
```
```
$ gulp
```

## Architecture
[Sails](http://sailsjs.org) application

"/api"

  |----> Containe models and controllers
  
         - Blueprint disabled, see custom routes.js and environment.js instead
  
         - Res/Req is a Rest application/json

"/views"

  |----> Containe [React/Flux](https://facebook.github.io/flux/docs/overview.html) architecture

         - When runing ```$ gulp``` browserify bundle main.js and "/views" into /assets

         - see /gulpfile.js for more details.

## Model
Currently, data is stored on Hard Driver using [sails-disk](https://github.com/balderdashy/sails-disk)
;