module.exports = {

  attributes: {
    id: {
      type: 'integer',
      primaryKey: true,
      unique: true,
      required: true
    },
    sku: {
      type: 'string',
      size: 10,
      unique: true,
      required: true
    },
    title: {
      type: 'string',
      required: true
    },
    summary: {
      type: 'string'
    },
    sale_price: {
      type: 'float'
    },
    purchase_price: {
      type: 'float'
    },
    quantity: {
      type: 'integer'
    },
    image: {
      type: 'string'
    },
    category: {
      type: 'string'
    },
    published: {
      type: 'boolean'
    },
  }
  
}
