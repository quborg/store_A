module.exports = {
  // ALL action
  index: function(req, res, next) {
    var where = req.param('where');
    if (_.isString(where)) {
      where = JSON.parse(where);
    }
    var options = {
      limit: req.param('limit') || undefined,
      skip: req.param('skip') || undefined,
      sort: req.param('sort') || undefined,
      where: where || undefined
    };
    Product.find(options, function(err, products) {
      if (products === undefined) return res.notFound();
      if (err) return next(err);
      res.json(products);
    });
  },
  // CREATE action  
  create: function(req, res, next) {
    var params = req.params.all();
    Product.create(params, function(err, product) {
      if (err) return next(err);
      res.status(201);
      res.json(product);
    });
  },
  // a FIND action
  find: function(req, res, next) {
    var id = req.param('id');
    Product.findOne(id, function(err, product) {
      if (product === undefined) return res.notFound();
      if (err) return next(err);
      res.json(product);
    });
  },
  // an UPDATE action
  update: function(req, res, next) {
    var criteria = {};
    criteria = _.merge({}, req.params.all(), req.body);
    var id = req.param('id');
    if (!id) {
      return res.badRequest('No id provided.');
    }
    Product.update(id, criteria, function(err, product) {
      if (product.length === 0) return res.notFound();
      if (err) return next(err);
      res.json(product);
    });
  },
  // a DESTROY action
  destroy: function(req, res, next) {
    var id = req.param('id');
    if (!id) {
      return res.badRequest('No id provided.');
    }
    Product.findOne(id).done(function(err, result) {
      if (err) return res.serverError(err);
      if (!result) return res.notFound();
      Product.destroy(id, function(err) {
        if (err) return next(err);
        return res.json(result);
      });
    });
  },
  // CONFIGURATION
  // _config: {}
}
