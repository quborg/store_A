'use strict'

var Store = require('../stores/Products')

function onRowSelect(row, isSelected){
  console.log(row)
  console.log("selected: " + isSelected)
}
function onSelectAll(isSelected){
  console.log("is select all: " + isSelected)
}
function onAfterSaveCell(row, cellName, cellValue) {
  console.log("Save cell '"+cellName+"' with value '"+cellValue+"'")
  console.log("Thw whole row :")
  console.log(row)
}

module.exports = {
  getProducts: function() { return { products: Store.getProducts() } },
  selectRowProp: {
    mode: "checkbox",
    clickToSelect: true,
    bgColor: "rgba(51, 122, 183, 0.7)",
    onSelect: onRowSelect,
    onSelectAll: onSelectAll
  },
  cellEditProp: {
    mode: "dbclick",
    blurToSave: true,
    afterSaveCell: onAfterSaveCell
  }
}
