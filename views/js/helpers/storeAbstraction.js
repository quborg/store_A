'use strinct'

var jQuery = require('jquery'),
    Actions = require('../actions/Actions'),
    _products = []

module.exports = {

  get_products: function() {
    return _products
  },

  set_products: function(p) {
    if(p) _products = p
  },

  getProducts: function() {
    jQuery.ajax({
      url: 'http://localhost:3000/api/products',
      success: Actions.receiveProducts,
      error: Actions.receiveProducts(_products)
    })
  },

  getProductItem: function(id) {
    jQuery.ajax({
      url: 'http://localhost:3000/api/product/'+id,
      success: Actions.getProductItem,
      error: Actions.getProductItem(_products)
    })
  }

}
