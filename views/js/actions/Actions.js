var Constants = require('../constants/Constants'),
    Dispatcher = require('../dispatchers/Dispatcher')

module.exports = {
  receiveProducts:function(products) {
    Dispatcher.handleServerAction({
      actionType: Constants.RECEIVE_PRODUCTS,
      products: products
    })
  },
  getProductItem:function(product) {
    Dispatcher.handleServerAction({
      actionType: Constants.GET_PRODUCT,
      product: product
    })
  },
}
