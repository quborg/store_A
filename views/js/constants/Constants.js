module.exports = {
  actions: {
    RECEIVE_PRODUCTS: 'RECEIVE_PRODUCTS',
    GET_PRODUCT: 'GET_PRODUCT'
  },
}
