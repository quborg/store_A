'use strict'

var React = require('react'),
    ReactDOM = require('react-dom'),
    StoreAbstraction = require('./helpers/storeAbstraction'),
    Routes = require('./site/Routes')

StoreAbstraction.getProducts()

ReactDOM.render(Routes, document.getElementById('content'))
