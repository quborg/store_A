var Dispatcher = require('flux').Dispatcher,
    assign = require('object-assign')

module.exports = assign(new Dispatcher(), {

  handleViewAction: function(action) {
    let payload = {
      source: 'VIEW_ACTION',
      action: action
    }
    this.dispatch(payload)
  },
  handleServerAction: function(action) {
    let payload = {
      source: 'SERVER_ACTION',
      action: action
    }
    this.dispatch(payload)
  }

})
