var React = require('react'),
    Store = require('../stores/Products')

module.exports = function(callback) {
  return {
    getInitialState: function() {
      return callback(this)
    },
    componentWillMount: function() {
      Store.addChangeListener(this._onChange)
    },
    componentWillUnmount: function() {
      Store.removeChangeListener(this._onChange)
    },
    _onChange: function() {
      if (this.isMounted()) {
        this.setState(callback(this))
      }
    }
  }
}
