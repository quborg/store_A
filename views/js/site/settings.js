'use strict'

module.exports = {
  
  mainMenu: [
    { name: "products", title: "Products", to: "/products"  },
    { name: "invoices", title: "Invoices", to: "/invoices"  }
  ],

  Views: {
    Products: {
      thead: [
        { name: "sku",            title: "SKU"                            },
        { name: "title",          title: "Title"                          },
        { name: "sale_price",     title: "Price"                          },
        { name: "purchase_price", title: "Purchase Price",  hidden: true  },
        { name: "quantity",       title: "Quantity"                       },
        { name: "category",       title: "Category"                       }
      ]
    }
  }

}
