'use strict'

import { Router, Route, IndexRoute } from 'react-router'
import createBrowserHistory from 'history/lib/createBrowserHistory'

var React = require('react'),
    Template = require('../templates/Template'),
    Dashboard = require('../components/dashboard/Dashboard'),
    Products = require('../components/products/Products'),
    Invoices = require('../components/invoices/Invoices'),
    NotFoundPage = require('../templates/NotFoundPage'),
    history = createBrowserHistory()

module.exports = (
  <Router history={history}>
    <Route path="/" component={Template}>
      <IndexRoute component={Products}/>
      <Route path="/products" component={Products}/>
      <Route path="/invoices" component={Invoices}/>
      <Route path="*" component={NotFoundPage}/>
    </Route>
  </Router>
)
