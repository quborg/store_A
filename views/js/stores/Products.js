var Dispatcher = require('../dispatchers/Dispatcher'),
    Constants = require('../constants/Constants'),
    StoreAbstraction = require('../helpers/storeAbstraction'),
    assign = require('object-assign'),
    EventEmitter = require('events').EventEmitter,
    CHANGE_EVENT = 'change'

var AppStore = assign(EventEmitter.prototype, {
  emitChange: function() {
    this.emit(CHANGE_EVENT)
  },
  addChangeListener: function(callback) {
    this.on(CHANGE_EVENT, callback)
  },
  removeChangeListener: function(callback) {
    this.removeListener(CHANGE_EVENT, callback)
  },
  getProducts: function() {
    return StoreAbstraction.get_products()
  }
})

dispatcherIndex: Dispatcher.register(function(payload) {
  var action = payload.action
  switch (action.actionType){
    case Constants.RECEIVE_PRODUCTS: 
      StoreAbstraction.set_products(action.products)
      break
  }
  AppStore.emitChange()
  return true
})

module.exports = AppStore
