'use strict'

var React = require('react'),
    Site = require('../../site/settings'),
    Mixins = require('../../mixins/Mixins'),
    Helper = require('../../helpers/productsAbstraction')

import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table'

module.exports = React.createClass({
  mixins: [Mixins(Helper.getProducts)],
  render: function() {
    return (
      <BootstrapTable data={this.state.products} hover={true} condensed={true} pagination={true} selectRow={Helper.selectRowProp} cellEdit={Helper.cellEditProp} insertRow={true} editRow={true} deleteRow={true} search={true}>
        {Site.Views.Products.thead.map(
          (c,k) => { return (
            <TableHeaderColumn isKey={k?!1:!0} key={k} dataField={c.name} dataSort={true} hidden={c.hidden?!0:!1}>{c.title}</TableHeaderColumn>
            )}
        )}
      </BootstrapTable>
    )
  }
})
